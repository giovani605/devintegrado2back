"use strict"

const express = require('express');
const router = express.Router();
var banco = require("./banco");

var multer = require('multer')
var upload = multer({ dest: 'uploads/' })

var processador = require("./processarImagemAPI");

// nesse arquivo fica a rota que lida com os cartoes
//recuperar o dado do cartao
router.get("/", (req, res, next) => {
    banco.buscarImagens((dados) => {
        res.status(200).send({
            "mensagem": "ok",
            "dados": dados
        })
    });

});

// Salva a imagem
router.post("/upload", upload.fields([{ name: 'imgMatrix' }, { name: 'imgVetor' }]), (req, res, next) => {
    // recuperar parametros da imagem
    console.log("Cheguei aqui");
    var files = JSON.parse(JSON.stringify(req.files));
    var fileMatrix = files["imgMatrix"][0].filename;
    var fileVetor = files["imgVetor"][0].filename;
    console.log("Files:" + files);
    console.log("Nome do Arquivo matrix: " + fileMatrix);
    console.log("Nome do Arquivo Vetor: " + fileVetor);
    // usar esse arquivo para grava no banco
    var dados = JSON.parse(req.body["dados"]);
    console.log("interacao");
    console.log(JSON.parse(req.body["dados"]));


    processador.processarImagem(fileMatrix, fileVetor, {
        "interacao": dados["interacoes"],
        "nomeFinal": dados["nome"]
    });

    console.log("sucesso");
    res.status(200).send({ "mensagem": "OK" });



    // salvar a iamgem no file system
    // responde com mensagem
});


module.exports = router;